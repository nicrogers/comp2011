from app import db

sales = db.Table('sales', db.Model.metadata,
    db.Column('agent_id', db.Integer, db.ForeignKey('agents.agent_id')),
    db.Column('listing_id', db.Integer, db.ForeignKey('listings.listing_id'))
)

class Listings(db.Model):
    listing_id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(200), index=True)
    sale_date = db.Column(db.DateTime)
    sale_price = db.Column(db.Integer)
    owner_name = db.Column(db.String(100), index=True)
    owner_email = db.Column(db.String(100), index=True)
    owner_record = db.Column(db.String(500), index=True)
    agents = db.relationship('Agents',secondary=sales)

class Agents(db.Model):
    agent_id = db.Column(db.Integer, primary_key=True)
    f_name = db.Column(db.String(100), index=True)
    l_name = db.Column(db.String(100), index=True)
    email = db.Column(db.String(100), index=True)
    password = db.Column(db.String(100), index=True)
    logged_in = db.Column(db.Boolean)
    listings = db.relationship('Listings',secondary=sales)
