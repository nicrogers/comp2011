from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import logging
# from flask_admin import Admin

#sets application object
app = Flask(__name__)
#directs to configuration
app.config.from_object('config')
#create an instance of the database
db = SQLAlchemy(app)

logging.basicConfig(filename='demo.log', level=logging.DEBUG)

#create a Migrate instance
migrate = Migrate(app, db)

#imports views and models modules.
from app import views, models
