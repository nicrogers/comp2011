from flask_wtf import Form
from wtforms import StringField, TextAreaField, IntegerField, SelectMultipleField
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired, length

from .models import Listings, Agents


class AddListingForm (Form):
    address = StringField('Address:', validators=[DataRequired(), length(max=200)])
    sale_date = DateField('Sale Date:', validators=[DataRequired()])
    sale_price = IntegerField('Sale Price ($):', validators=[DataRequired()])
    owner_name = StringField('Owner Name:', validators=[DataRequired(), length(max=40)])
    owner_email = StringField('Owner Email:', validators=[DataRequired(), length(max=40)])
    choices = [(g.agent_id, g.f_name + ' ' + g.l_name) for g in Agents.query.order_by('l_name')]
    agents =  SelectMultipleField('Agents', coerce=int ,choices = choices)

class ChangePasswordForm (Form):
    new_password = StringField('New password:', validators=[DataRequired(), length(min=8)])
    confirm_new_password = StringField('Confirm new password:', validators=[DataRequired(), length(min=8)])

class UpdateListingForm (Form):
    record = TextAreaField('Owner contact record:', validators=[DataRequired()])

class LoginForm (Form):
    email = StringField('Email:', validators=[DataRequired()])
    password = StringField('Password:', validators=[DataRequired()])
