from flask import render_template, flash, redirect, url_for, session
from app import app, db, models
from .forms import AddListingForm, ChangePasswordForm, LoginForm, UpdateListingForm
from .models import Listings, Agents
import datetime, flask

@app.route('/')
def redirector():
    if 'logged_in' in session:
        return redirect(url_for('my_listings'))
    return redirect(url_for('sign_in'))

@app.route('/add_listing', methods=['GET', 'POST'])
def addlisting():
    if 'logged_in' in session:
        form = AddListingForm()
        if form.validate_on_submit():
            app.logger.info('%s form has been validated', form)
            newListing = Listings(address=form.address.data, sale_date=form.sale_date.data, sale_price=form.sale_price.data, owner_name=form.owner_name.data, owner_email=form.owner_email.data, owner_record="")
            for agentid in form.agents.data:
                agent = models.Agents.query.get(agentid)
                newListing.agents.append(agent)
            db.session.add(newListing)
            db.session.commit()
            app.logger.info('New listing for %s has been added.', newListing.address)
            flash('Listing has been added.')
        return render_template('add_listing.html',
                                   title='Add Listing',
                                   form=form)
    return redirect(url_for('sign_in'))

@app.route('/listings', methods=['GET'])
def listings():
    if 'logged_in' in session:
        listings = models.Listings.query.all()
        return render_template('listings.html', title='All Listings', listings=listings)
    return redirect(url_for('sign_in'))

@app.route('/my_listings', methods=['GET'])
def my_listings():
    if 'logged_in' in session:
        agents = models.Agents.query.all()
        for agent in agents:
            if agent.logged_in == True:
                return render_template('my_listings.html', title='My Listings', agent=agent)
    return redirect(url_for('sign_in'))

@app.route('/account', methods=['GET', 'POST'])
def account():
    if 'logged_in' in session:
        agents = models.Agents.query.all()
        for agent in agents:
            if agent.logged_in == True:
                form = ChangePasswordForm()
                if form.validate_on_submit():
                    app.logger.info('%s form has been validated.', form)
                    if form.new_password.data == form.confirm_new_password.data:
                        agent.password = form.confirm_new_password.data
                        db.session.commit()
                        app.logger.info('%s %s has updated their password.', agent.f_name, agent.l_name)
                        flash('Password has been updated.')
                    else:
                        flash('Error: Passwords do not match.')
                    if not form.new_password.data == None:
                        if len(form.new_password.data) < 8:
                            flash('Error: Password minimum length is 8 characters.')
                return render_template('account.html',
                                   title='Account',
                                   form=form, agent=agent)
    return redirect(url_for('sign_in'))

@app.route('/listing/<id>', methods=['GET', 'POST'])
def listing(id):
    if 'logged_in' in session:
        listing = models.Listings.query.get(id)
        form = UpdateListingForm()
        if flask.request.method == 'GET':
            form.record.data = listing.owner_record
        if form.validate_on_submit():
            app.logger.info('%s form has been validated.', form)
            listing.owner_record = form.record.data
            db.session.commit()
            app.logger.info('Owner record for %s listing has been updated.', listing.address)
            flash('Owner record has been updated.')
        return render_template('listing.html',
                                   title='Listing', listing=listing, agents=listing.agents, form=form)
    return redirect(url_for('sign_in'))

@app.route('/sign_in', methods=['GET', 'POST'])
def sign_in():
        form=LoginForm()
        if form.validate_on_submit():
            app.logger.info('%s form has been validated.', form)
            agents = models.Agents.query.all()
            for agent in agents:
                if agent.email == form.email.data and agent.password == form.password.data:
                    agent.logged_in = True
                    db.session.commit()
                    listings = models.Listings.query.all()
                    session['logged_in'] = True
                    app.logger.info('%s %s has logged in.', agent.f_name, agent.l_name)
                    return redirect(url_for('my_listings'))
            flash('Incorrect email or password.')
        return render_template('sign_in.html',
                               title='Sign In',
                               form=form)

@app.route('/sign_out', methods=['GET', 'POST'])
def sign_out():
    agents = models.Agents.query.all()
    for agent in agents:
        if agent.logged_in == True:
            agent.logged_in = False
            db.session.commit()
            app.logger.info('%s %s has logged out.', agent.f_name, agent.l_name)
    session.pop('logged_in',None)
    flash('Sign out successful.')
    return redirect(url_for('sign_in'))
